rs.initiate({
  _id: "rs0",
  configsvr: true,
  members: [
    { _id: 0, host: "config_node_1:27019" },
    { _id: 1, host: "config_node_2:27019" },
    { _id: 2, host: "config_node_3:27019" }
  ]
});
