rs.initiate({
  _id: "rs1",
  members: [
    { _id: 0, host: "shard_node_1:27018" },
    { _id: 1, host: "shard_node_2:27018" },
    { _id: 2, host: "shard_node_3:27018" },
    { _id: 3, host: "shard_node_4:27018", priority: 0, hidden: true }
  ]
});
