#!/bin/bash

while :
do
  timestamp=$(date +%s)

  docker exec mongos_node_1 bash -c "mongo ask_db --eval \"db.test.insert({ test_id: \"$timestamp\" }); db.test.find({ test_id: \"$timestamp\" });\""

  sleep 1
done
