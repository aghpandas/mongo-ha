# Mongo RS with sharding configuration

## Requirements

- [Docker](https://docs.docker.com/docker-for-mac/install/)

## Initialize

Build/run docker nodes:

```
docker-compose up --build
```

## Configure

Configuration script:

- Initialises a replica set for shard config server
- Initialises a replica set with sharding using the config server
- One of the shard nodes (shard_node_4) is a hidden replica member
- Initialises a mongos client that uses config server to connect with shards
- Initialises sharded db "ask_db" and sharded collection "ask_db.test"
- The collection uses hash key as shard key

```
./configure.sh
```

## Client

Connect as client with primary discovery from mongos_node_1

```
docker exec -it mongos_node_1 mongo ask_db
```

## Traffic simulation

Simulation inserts a new record every one second with a UNIX timestamp as key

```
./simulation.sh
```

## Failover test

- Connect to one of the shards:

```
docker exec -it shard_node_1 bash -c 'mongo --port 27018'
```

- Check what nodes have what roles:

```
rs.status()
```

- Let's say we want to test master failover, then find which node holds this role now (e.g. shard_node_3) and do:

```
docker stop shard_node_3
```

- You should observe the failover events in the logs from docker-compose:

```
shard_node_1     | 2019-04-27T11:01:04.329+0000 I REPL     [replexec-3] transition to PRIMARY from SECONDARY

...

shard_node_2     | 2019-04-27T11:01:04.519+0000 I REPL     [replexec-44] Member shard_node_1:27018 is now in state PRIMARY

...

shard_node_2     | 2019-04-27T11:01:04.828+0000 I REPL_HB  [replexec-44] Error in heartbeat (requestId: 11793) to shard_node_3:27018, response status: HostUnreachable: Error connecting to shard_node_3:27018 (192.168.112.7:27018) :: caused by :: Connection refused

...

shard_node_2     | 2019-04-27T11:01:04.832+0000 I REPL     [replexec-47] Member shard_node_3:27018 is now in state RS_DOWN
```

- As well as from the simulation logs. notice the difference between the printed timestamps. In this case
  between 1556363102 and 1556363116 which mean the time between killing a master and the DB accepting writes again there was 14 seconds delay:

```
MongoDB shell version v4.0.9
connecting to: mongodb://127.0.0.1:27017/ask_db?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("bb677d74-8682-4ed6-87da-4e1275e78337") }

MongoDB server version: 4.0.9
{ "_id" : ObjectId("5cc4375de238fd5b4ceac6a4"), "test_id" : 1556363101 }
MongoDB shell version v4.0.9
connecting to: mongodb://127.0.0.1:27017/ask_db?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("971f39ca-3724-4e28-9b70-82269715758b") }

MongoDB server version: 4.0.9
{ "_id" : ObjectId("5cc4375eb6ec8855ae259255"), "test_id" : 1556363102 }
MongoDB shell version v4.0.9
connecting to: mongodb://127.0.0.1:27017/ask_db?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("7a7e1b49-3530-4ece-9549-c322f39cd2e0") }

MongoDB server version: 4.0.9
MongoDB shell version v4.0.9
connecting to: mongodb://127.0.0.1:27017/ask_db?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("0960afb8-aabd-414c-851f-f25c61fc2d51") }

MongoDB server version: 4.0.9
{ "_id" : ObjectId("5cc4376c531f447ecfd6f3aa"), "test_id" : 1556363116 }
MongoDB shell version v4.0.9
connecting to: mongodb://127.0.0.1:27017/ask_db?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("b65c0ed4-9377-42ae-8a95-e9321ffe53f1") }

MongoDB server version: 4.0.9
{ "_id" : ObjectId("5cc4376d438c6d9af2f8b649"), "test_id" : 1556363117 }
MongoDB shell version v4.0.9
connecting to: mongodb://127.0.0.1:27017/ask_db?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("015a151e-c0ff-4895-94fe-f11a2a5b2265") }

MongoDB server version: 4.0.9
{ "_id" : ObjectId("5cc4376e662da6e82678b4a2"), "test_id" : 1556363118 }
```

- To start the node again:

```
docker start shard_node_3
```
