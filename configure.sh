#!/bin/bash

# Initialise RS for config server
docker exec config_node_1 bash -c 'mongo --port 27019 < /data/config/config_replica.js'

# Initialise RS for sharding
docker exec shard_node_1 bash -c 'mongo --port 27018 < /data/config/shard_replica.js'

# Add shards
docker exec mongos_node_1 bash -c 'mongo --eval "sh.addShard(\"rs1/shard_node_1:27018\")"'

# Enable sharding for database
docker exec mongos_node_1 bash -c 'mongo --eval "sh.enableSharding(\"ask_db\")"'

# Enable sharding for test collection
docker exec mongos_node_1 bash -c 'mongo --eval "sh.shardCollection(\"ask_db.test\", { test_id: \"hashed\" })"'
